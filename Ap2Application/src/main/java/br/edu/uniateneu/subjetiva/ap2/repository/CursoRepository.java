package br.edu.uniateneu.subjetiva.ap2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.subjetiva.ap2.modelo.Curso;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long>{
	

}
